<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/public/partials
 */ 
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div id="awsm-cookie-notice">

  <h3 class="awsm-cookie-notice__title">
    <?php if ( get_field( 'awsm_cookie_notice__title', 'option' ) ):
      // This is displayed when the field_name is TRUE or has a value.
      the_field('awsm_cookie_notice__title', 'option');
        else: // This is displayed when the field is FALSE, NULL or the field does not exist.
        _e('Privacy Policy','awsm'); ?> 

    <?php endif; // end of if field_name logic ?>
  </h3> 

  <p class="awsm-cookie-notice__text">
    <?php if ( get_field( 'awsm_cookie_notice__text', 'option' ) ): 

      // This is displayed when the field_name is TRUE or has a value.
      the_field('awsm_cookie_notice__text', 'option');

        else: // This is displayed when the field is FALSE, NULL or the field does not exist.
        _e('We use cookies.','awsm'); ?> 

    <?php endif; // end of if field_name logic ?>
  </p>

  <!-- Button: Link to Privacy Policy Page -->
  <p class="awsm-cookie-notice__link">
    <a href="<?php echo esc_url(get_permalink(get_option('wp_page_for_privacy_policy'))); ?>">
    
    <?php if ( get_field( 'awsm_cookie_notice__link', 'option' ) ): 

// This is displayed when the field_name is TRUE or has a value.
the_field('awsm_cookie_notice__link', 'option');

  else: // This is displayed when the field is FALSE, NULL or the field does not exist.
  _e('Read the privacy policy ','awsm'); ?> 

  <?php endif; // end of if field_name logic ?>

    <span class="fas fa-arrow-right"></span></a>
  </p>

  <p class="awsm-cookie-notice__action">
    <!-- Button: Accept cookies -->
    <button id="awsm-cookie-notice__button">
      <a href="#">
      <?php if ( get_field( 'awsm_cookie_notice__button', 'option' ) ): 

// This is displayed when the field_name is TRUE or has a value.
the_field('awsm_cookie_notice__button', 'option');

  else: // This is displayed when the field is FALSE, NULL or the field does not exist.
  _e('Accept cookies','awsm'); ?> 

<?php endif; // end of if field_name logic ?>
      </a>
    </button>
  </p>

</div>
