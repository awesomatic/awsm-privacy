(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 // Code goes here

    $(document).ready(function() {
          //Get current time
          var currentTime = new Date().getTime();
          //Add hours function
          Date.prototype.addHours = function(h) {
             this.setTime(this.getTime() + (h*60*60*1000));
             return this;
          }
          //Get time after 24 hours
          var after24 = new Date().addHours(10).getTime();
          //Hide div click
          $('#awsm-cookie-notice__button').click(function(){
              //Hide div
              $('#awsm-cookie-notice').hide();
              //Set desired time till you want to hide that div
              localStorage.setItem('desiredTime', after24);
          });
          //If desired time >= currentTime, based on that HIDE / SHOW
          if(localStorage.getItem('desiredTime') >= currentTime)
          {
              $('#awsm-cookie-notice').hide();
          }
          else
          {
              $('#awsm-cookie-notice').show();
          }
  });

  //clear data function

  function clearClick(number){
  	localStorage.clear();
  }

})( jQuery );
