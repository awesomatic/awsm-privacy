<?php

/**
 * Fired during plugin deactivation
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.5
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awesomatic_Privacy_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5
	 */
	public static function deactivate() {

	}

}
