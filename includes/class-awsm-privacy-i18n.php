<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.5
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awesomatic_Privacy_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.5
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-privacy',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
