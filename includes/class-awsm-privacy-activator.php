<?php

/**
 * Fired during plugin activation
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.5
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awesomatic_Privacy_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5
	 */
	public static function activate() {

	}

}
