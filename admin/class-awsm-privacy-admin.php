<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/admin
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awesomatic_Privacy_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.5
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.5
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.5
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.5
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesomatic_Privacy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesomatic_Privacy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/awsm-privacy-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.5
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesomatic_Privacy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesomatic_Privacy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/awsm-privacy-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates the options page
	 *
	 * @since 		0.5
	 * @return 		void
	 */
	public function page_options() {

		if( function_exists('acf_add_options_page') ) {
			
			acf_add_options_sub_page(array(
				'page_title'     => 'Cookiebanner',
				'menu_title'    => 'Cookiebanner',
				'parent_slug'    => 'options-general.php',
			));
			
		}

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_5e6f6e29c93f6',
				'title' => 'Cookiebanner',
				'fields' => array(
					array(
						'key' => 'field_5e6f6e4611175',
						'label' => 'Titel',
						'name' => 'awsm_cookie_notice__title',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Privacy Policy',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5e722587bf2f9',
						'label' => 'Tekst',
						'name' => 'awsm_cookie_notice__text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'We use cookies',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5e7226b936f2e',
						'label' => 'Link',
						'name' => 'awsm_cookie_notice__link',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Read the privacy policy',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5e7229223d907',
						'label' => 'Button',
						'name' => 'awsm_cookie_notice__button',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Accept cookies',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5e7d243860728',
						'label' => 'Cookiebanner inschakelen',
						'name' => 'awsm_cookie_notice_switch',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 1,
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'acf-options-cookiebanner',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));
			
			endif;
			
	}

}
