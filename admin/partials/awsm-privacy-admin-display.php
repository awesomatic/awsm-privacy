<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awesomatic_Privacy
 * @subpackage Awesomatic_Privacy/admin/partials
 */

 ?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
